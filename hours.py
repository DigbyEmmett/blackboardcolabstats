import sys
class person:
   def __init__(self,name:str,username:str):
      self.name = name
      self.username = username
      self.time = 0

   def addTime(self,time:str):
      """
         converts a string time into an integer and adds it to the total time
      """ 

      timeparts = time.split(":")
      timeparts = [int(i) for i in timeparts]
      # the line before converts every item inside the timeparts list to an
      # interger and then makes a new list from it

      try:
         hours = timeparts[0] 
         minutes = timeparts[1] 
         seconds = timeparts[2] 
         # slicing lists
         # a = [1,2,3,4,5,6]
         # we want [ 3 4 5 ]
         # b = a[2:-2] OR a[2:4]
      except IndexError as e:
         raise ValueError("Invalid Time :(")
      else:
         self.time += hours
         self.time += minutes / 60
         self.time += seconds / (60 * 60)

   def __str__(self):
      return self.name + " " + str(self.time)
      
def readCSV(filename,person_dictionary):
   fileobj = open(filename)
   lines = fileobj.readlines()
   header = lines[0]
   print(header)
    
   for i in range(1,len(lines)):
      line = lines[i]
      data = line.split(",")
      name = data[0]
      username = data[1]
      totaltime = data[6]
      if(username in person_dictionary):
         p = person_dictionary[username]
      else:
         new_person = person(name,username)
         p = person_dictionary[username] = new_person
      p.addTime(totaltime)
      
my_dict = {}
for i in sys.argv[1:]:
   readCSV(i,my_dict)

for i in my_dict.keys():
   person = my_dict[i]
   print(person)
